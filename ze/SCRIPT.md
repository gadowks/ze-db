###########################
#### Configuracion DB #####
###########################
> Install composer
```sh
/c/xampp/php/php.exe -r "copy('https://getcomposer.org/installer','composer-setup.php');"

/c/xampp/php/php.exe -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

/c/xampp/php/php.exe composer-setup.php

/c/xampp/php/php.exe -r "unlink('composer-setup.php');"

##composer update --no-scripts
```

> Create project
```sh
composer create-project zendframework/zend-expressive-skeleton expressive
```

> Install adaptador DB
```sh
/d/wamp64/bin/php/php7.1.9/php.exe composer.phar require zendframework/zend-db
```

> check \config\config.php
```php
$aggregator = new ConfigAggregator([
   \Zend\Db\ConfigProvider::class
])
```

> create \config\autoload\db.global.php
```php
return [
    'db' => [
        'driver' => 'Pdo',
        'dsn'      => 'mysql:dbname=siar_db;host=127.0.0.1;port=3306;',
        'user'     => 'root',
        'password' => ''
    ],
];
```

> Modify \config\routes.php //Lib route que soporta a la vez [POST,GET, ...]
```php
use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;
```

> Modify \config\routes.php //Establecer ruta de handler
```php
    $app->route(    '/rei/asistencia', 
                    [
                        BodyParamsMiddleware::class,
                        App\Handler\AsistenciaPageHandler::class
                    ], 
                    ['GET', 'POST'], 
                    'api.books'
                );
```

> Modify \src\App\src\ConfigProvider.php //Vincular handler con factory
```php
public function getDependencies() : array
    {
        return [
            'factories'  => [
                Handler\AsistenciaPageHandler::class => Handler\AsistenciaPageHandlerFactory::class,
            ],
        ];
    }
```

> Modify App\src\Handler\HandlerFactory.php //Llamar libreria Zend DB AdapterInterface && __invoke() enviar desde factory el objeto AdapterInterface al handler
```php
use Zend\Db\Adapter\AdapterInterface;
```
```php
public function __invoke(ContainerInterface $container) : RequestHandlerInterface
{
    $adapter = $container->has(AdapterInterface::class)? 
                    $container->get(AdapterInterface::class) : null; //<<--

    return new AsistenciaPageHandler(
                    get_class($container), 
                    $router, 
                    $template,
                    $adapter //<<--
                );
}
```

> Modify App\src\Handler\Handler.php 
> //Llamar libreria Zend DB Adapter && __construct declarar objeto adapter
```php
//use Zend\Db\Adapter\Adapter;
```
```php
class AsistenciaPageHandler implements RequestHandlerInterface
{
 private $adapter;
 public function __construct(
        $adapter
    ) {
        $this->adapter       = $adapter;
    }
}
```
```php
 public function handle(ServerRequestInterface $request) : ResponseInterface
{
    $select     = $this->adapter->query('select * from user');
    $result     = $select->execute();
    $data       = $result->getResource()->fetchAll();
    
    return new JsonResponse(
        $data
    );
 }
```

> //Documentation
* https://zendframework.github.io/zend-db/result-set/
























