<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

//::::::::::::: Zend DB
use Zend\Db\Adapter\Adapter;

class AsistenciaPageHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    //::::::::::::: Zend DB
    private $adapter;

    public function __construct(
        string $containerName,
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null,
        //::::::::::::: Zend DB
        $adapter
    ) {
        $this->containerName = $containerName;
        $this->router        = $router;
        $this->template      = $template;
        //::::::::::::: Zend DB
        $this->adapter       = $adapter;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {        
        $select     = $this->adapter->query('select * from user');
        $result     = $select->execute();
        $data       = $result->getResource()->fetchAll();

        return new JsonResponse(
            $data
        );                    
    }
}
