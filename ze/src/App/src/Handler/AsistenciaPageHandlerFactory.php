<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

//::::::::::::: Zend DB
use Zend\Db\Adapter\AdapterInterface;

use function get_class;

class AsistenciaPageHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $router   = $container->get(RouterInterface::class);
        $template = $container->has(TemplateRendererInterface::class)? $container->get(TemplateRendererInterface::class) : null;
       
        //::::::::::::: Zend DB
        $adapter = $container->has(AdapterInterface::class)? $container->get(AdapterInterface::class) : null;

        return new AsistenciaPageHandler(
                        get_class($container), 
                        $router, 
                        $template,
                        $adapter
                    );
    }
}
